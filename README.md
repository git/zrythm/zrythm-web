# zrythm-web

Website source for https://www.zrythm.org

This project was forked from the GNUnet website source code,
which was licensed under the GPLv3 and includes all Copyright notices of the
original authors.

The original README is preserved as README.GNUnet

# Building
`make` will create the full bundle in `rendered`

----

Copyright (C) 2019 Alexandros Theodotou

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
